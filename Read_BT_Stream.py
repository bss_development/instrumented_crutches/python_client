import socket
import struct
import math
import csv
import libscrc
from datetime import datetime


csv_header = ['counter', 'millis', 'acceleration X', 'acceleration Y', 'acceleration Z',
              'magnetometer X', 'magnetometer Y', 'magnetometer Z',
              'gyroscope X', 'gyroscope Y', 'gyroscope Z', 'force X', 'force Y', 'force Z',
              'torque X', 'torque Y', 'torque Z']

# fill in the mac-address of the device you're using
sensorMACAddress = 'E0:E2:E6:70:86:7E' #Force Amp A
#sensorMACAddress = 'E0:E2:E6:70:36:1A' #Force Amp B

port = 1

s = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)
s.connect((sensorMACAddress, port))
s.settimeout(8)  #non-blocking after 8 seconds
#s.send(bytes('<stop>', 'UTF-8'))  # Send command to BT client
# insert delay here? (delay(500))
s.send(bytes('<battery>', 'UTF-8'))  # Send command to BT client

# offsets send to a DAC, 12 bit value max, no offset is 2048, 0 max negative offset, 4095 is max positive offset. Do not send values higher than 4095!
# command format is <offset 1111 2222 3333 4444 5555 6666 endoffsets>   (without the spaces). #e.g. ('<offset267040944094409440944094endoffsets>', 'UTF-8'))

offset = {1000, 1001, 1002, 1003, 1004, 1005} #adjust here the offset values per Force/Torque channel

offsetsString = ''.join(str(e).rjust(4, '0') for e in offset)

startOffsetCommand = "offset"
stopOffsetCommand = "endoffsets"

offsetList = ["<", startOffsetCommand, offsetsString, stopOffsetCommand, ">"]
offsetCommandString  = ''.join(offsetList)

s.send(bytes(offsetCommandString, 'UTF-8'))  # Send command to BT client
s.send(bytes('<start>', 'UTF-8'))  # Send command to BT client


previousByte = -1
isReadingPacket = False
sampleSize = 40
btReadSize = 4096

counterBytePos = 2
millisBytePos = 5
accelBytePos = 8
magBytePos = 14
gyroBytePos = 20
forceBytePos = 26
crcBytePos = 38

#conversion for MPU9250 ( And MPU6886 ? )
conversion_factor_accel = 16 / 32768    #to g
conversion_factor_mag = (((( 170 - 128) * 0.5) / 128) + 1)  #to µT
d2r = 3.14159265359/180.0 #deg to radian
conversion_factor_gyro = 2000 / 32767.5 * d2r   #to rad/s  (gyro scale 2000DPS)

stream = bytearray(b'\xff\xfa')
startByte = bytearray(b'\xff\xfa')

# initialize lists and variables
sensorOutputCounter = ([])
sensorOutputMillis = ([])
sensorOutputAccel = ([])
sensorOutputMag = ([])
sensorOutputGyro = ([])
sensorOutputForce = ([])
totalSampleList = []

counter = 0
millis = 0
millisOld = 0 
accel = [0, 0, 0]
mag = [0, 0, 0]
gyro = [0, 0, 0]

crc_received = 0  #stores the crc calculated on the embedded device side
crc_calculated = 0  #stores the crc calculated on the PC side

buffer = bytearray(b'\x00')

chunk_size = 1

date = datetime.now().strftime("%Y%m%d_%I%M%S")
sensorNo = sensorMACAddress[-2:]

with open(f'Sensor{sensorNo}_Date{date}.csv', 'w', newline='') as csvfile: 
    csv_writer = csv.writer(csvfile)
    csv_writer.writerow(csv_header)
            
# unpack the sensor data from bytes to floats
def unpack_sensor_data(stream):
    
    global counter
    counterOld = counter
    counter =  int.from_bytes(stream[counterBytePos:counterBytePos+3], 'little') 

    if(counter - counterOld != 1):
        print(now, ": ", "Error: current Counter: ", counter, " previous counter: ", counterOld, " jump: ", int(counter - counterOld) )

    global millisOld
    
    millis =  int.from_bytes(stream[millisBytePos:millisBytePos+3], 'little')  

    accel = struct.unpack('hhh', stream[accelBytePos:accelBytePos+6]) 
    mag = struct.unpack('hhh', stream[magBytePos:magBytePos+6]) 
    gyro = struct.unpack('hhh', stream[gyroBytePos:gyroBytePos+6]) 

    accel_converted = [0.0, 0.0, 0.0]
    mag_converted = [0.0, 0.0, 0.0]
    gyro_converted = [0.0, 0.0, 0.0]
    force = [0, 0, 0, 0, 0, 0]

    #convert int to real units 
    for i in range(0,3):
        accel_converted[i] = float(accel[i]) * conversion_factor_accel
        mag_converted[i] = float(mag[i]) * conversion_factor_mag
        gyro_converted[i] = float(gyro[i]) * conversion_factor_gyro


    force = struct.unpack('hhhhhh', stream[forceBytePos:forceBytePos+12]) #2bytes * 6DoF force info = 12
    totalSample = [counter] + [millis] + accel_converted + mag_converted + gyro_converted + [force[0]] + [force[1]] + [force[2]] + [force[3]] + [force[4]] + [force[5]]
    
    
    if(millis - millisOld > 1000):
        millisOld = millis 
        print(now, ": counter", counter, ",  millis: ", millis, "accel_x: ", accel_converted[0], "gyro_x: ", gyro_converted[0], "force: ", force[0]," | ", force[1]," | ", force[2]," | ", force[3]," | ", force[4]," | ", force[5])


    totalSampleList.append(totalSample)
    
    
    if counter % 1000 == 0: 
        with open(f'Sensor{sensorNo}_Date{date}.csv', 'a', newline='') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=',')
            for row in totalSampleList:
                csv_writer.writerow(row)
        
        totalSampleList.clear()
        print('now the list is saved to csv and the content is cleared')
        

now = datetime.today().timestamp()  # Get timezone naive now
startTime = int( now )
currentTime = int(now)

position = 0 #seek position through the raw data array
newPosition = 0 

while True:
    try: 
        now = datetime.today().timestamp()  # Get timezone naive now
        currentTime = int(now)
        data = s.recv(btReadSize) #read bluetooth data
    
        now = datetime.now().time() # time object
        
        if data:
            buffer += data
            # print(now, ": ", len(buffer)/40)
            
        else:
            print(now, ": ", "no data read, stopping")
            s.connect((sensorMACAddress, port))
    
            s.send(bytes('<stop>', 'UTF-8'))  # Send command to BT client
            s.close()
            break
        
    
        #buffer.seek(0, 2)  # Seek the end
        num_bytes = len(buffer)  # Get the buffer size
    
        #from example extract_pngs.py    (https://www.devdungeon.com/content/working-binary-data-python)
        count = 0
      
    
        for i in range(0, math.ceil(btReadSize/sampleSize)):
            newPosition = buffer.find(startByte, position+1, num_bytes - sampleSize)
            if newPosition != -1:
                oneRawSample = buffer[position:position+sampleSize]
    
                #read crc provided by device
                crc_received = int.from_bytes(oneRawSample[crcBytePos:crcBytePos+2], 'little')
                
                #calculate crc 
                crc_calculated = libscrc.ccitt_false(oneRawSample[0:sampleSize-2])  #libscrc.ccitt_false uses 0xFFFF as init byte
                #compare calculated and received CRC
                
                if(crc_received == crc_calculated):
                    unpack_sensor_data(oneRawSample) #convert raw bytes to decimal values of the individual channels
                #else:
    
                    #print(now, ": ", "startPosition: ", newPosition, " crc received: ", crc_received, " crc calculated: ", crc_calculated )
                
                position = newPosition  #move the pointer such that the beginning of the buffer is not parsed again
    
    except KeyboardInterrupt:  
        with open(f'Sensor{sensorNo}_Date{date}.csv', 'a', newline='') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=',')
            for row in totalSampleList:
                csv_writer.writerow(row)
                
        s.send(bytes('<stop>', 'UTF-8'))  # Send command to BT client
        s.close()



