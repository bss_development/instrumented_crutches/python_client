# Python_client
simple client to connect to the embedded device using Bluetooth and collect the sensor data

## Description
collects data from embedded device made for ArmCoach4Stroke and Brafo. 

effectively it's a port of the Matlab script Instrumented_Object_Aquisition.m  (  https://gitlab.utwente.nl/bss_development/armcoach4stroke_instrumented_object/-/tree/Matlab_files )

In a while loop it reads data from BT socket. Then searches for start bytes in the received data. 
When start bytes are found, a CRC check is performed. 
If the calculated CRC matches the CRC send by the device, the data is send to "unpack_sensor_data".
This function extracts the different channels from the raw byte data, and appends it to lists per channel type.

The lists are aquired in memory and stored in an CSV ASCII file after aquisition is finished.

CSV content:
line 1: counter, counter, counter...
line 3: millis, millies, millies...
line 5: "[accel_x, accel_y, accel_z]", "[accel_x, accel_y, accel_z]", "[accel_x, accel_y, accel_z]"...
line 7: "[mag_x, mag_y, mag_z]", "[mag_x, mag_y, mag_z]", "[mag_x, mag_y, mag_z]"...
line 9: "[gyro_x, gyro_y, gyro_z]", "[gyro_x, gyro_y, gyro_z]", "[gyro_x, gyro_y, gyro_z]"....
line 11: "[force_1, force_2, force_3, force_4, force_5, force_6] ", "[force_1, force_2, force_3, force_4, force_5, force_6] ", "[force_1, force_2, force_3, force_4, force_5, force_6] "

This is far from efficient...

## Installation
Requires at least Python 3.9 to connect to Bluetooth via Socket
Requires Numpy and Matplotlib 

CRC check is based upon https://pypi.org/project/libscrc/  install via 
pip3 install libscrc


## Usage
Pair the sensor in Windows (add Bluetooth Devices)

adjust sensorMACAddress to match the MAC address of the sensor you want to use
- 6DOF_Force_01 -->  sensorMACAddress = 'AC:67:B2:09:58:7A''
- 6DOF_Force_02 -->  sensorMACAddress = 'E0:E2:E6:70:86:7E'
- 6DOF_Force_03 -->  sensorMACAddress = 'E0:E2:E6:70:36:1A'


mac address in Windows can be found via Bluetooth icon, "Show Bluetooth Devices", "More Bluetooth Options" on the right, "Hardware" tab, double click on the device.
go to "Details" and select "Hardware IDs", the last part of "BTHENUM\Dev_*" is the MAC.


adjust aquisitionDuration to the number of seconds you want the script to run.

## Support
f.muijzer@utwente.nl

## Project status
Current limitations:

- no way to stop script during aquisition. Killing it often leaves the socket broken. If you cannot reconnect, you have to power cycle the embedded device.
- only at the end, the data is saved to CSV, a crash will lead in data loss.
- negative values are read incorrectly.
- lists with sensor data grow indefinately, making it slower over time.
- 